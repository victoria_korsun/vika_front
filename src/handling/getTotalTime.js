module.exports = function getTotalTime(str) {
    const regex = /resptime:"(?<time>[\d, .]+)"/mg;
    return str.match(regex).map(item => {
        return item.replace(/[^0-9.,]/mg, '');
    }).join(',').split(',').reduce((total, amount) => +total + +amount);
};
