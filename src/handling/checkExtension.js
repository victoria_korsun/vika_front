module.exports = (file) => {
    return (file.split('.').pop() === 'log');
};