const express = require('express');
const path = require('path');
const fs = require('fs');
const getTime = require('./handling/getTotalTime');
const isLogFile = require("./handling/checkExtension");

const app = express();
const PORT = process.env.PORT ?? 8080;

app.get('/:some', (req, res) => {
    res.send("<h1>Sorry, this request is not correct.</h1>")
});

app.get('/logs/:name/', (req, res) => {
    const {params: {name}} = req;

    if (isLogFile(name)) {
        fs.promises.readFile(path.resolve(`./var/www/vika/${name}`))
            .then(data => {
                const totalTime = getTime(data.toString());
                res.send(`
                    <style>
                        @import url('https://fonts.googleapis.com/css2?family=Roboto+Mono&display=swap');
                    </style>
                    <body style="height: 100vh; margin: 0">
                        <div style="height: 100vh; display: flex; justify-content: center; align-items: center; flex-direction: column">
                            <h1 style="font-family: Roboto Mono,monospace; color: green">Total time</h1>
                            <h3 style="font-family: Roboto Mono,monospace">${totalTime}</h3>
                        </div>
                `);
            }).catch(err => {
            res.status(404).sendFile(path.resolve("./html/error404.html"));
        });
    } else {
        res.status(502).sendFile(path.resolve("./html/error502.html"))
    }
});

app.listen(PORT, () => {
    console.log(`Server has been started on port ${PORT}...`)
});