FROM node:16.14.0

WORKDIR /app

COPY package-lock.json .

COPY package.json .

RUN npm ci

COPY /src .

COPY /logs ./var/www/vika

EXPOSE 8080

CMD ["node", "index.js"]
