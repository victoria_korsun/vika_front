## Docker

### Build & Dockerfile

To build the image you can use the following command

```bash
docker build --tag vkorsun/test-server .
```
### Run

After building the image you can simply run it with

```bash
docker run -p 8080:8080 vkorsun/test-server
```


